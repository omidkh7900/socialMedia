<?php


return [
    'bio' => 'bio',
    'following' => 'following',
    'followers' => 'followers',
    'show' => [
       'posts' => 'posts',
       'tags' => 'tags',
       'savePosts' => 'saved',
    ],
    'choose' => 'Choose...',
    'fullName' => 'Full Name',
    'password' => 'Password',
    'username' => 'Username',
    'country' => 'Country',
    'countries' => [
        'iran' => 'Iran',
    ],
    'state' => 'State',
    'states' => [
        'tehran' => 'Tehran',
        'khorasanRazavi' => 'Khorosan Razavi',
    ],
    'city' => 'City',
    'cities' => [
        'tehran' => 'Tehran',
        'sabzevar' => 'Sabzevar',
    ],
    'upload' => 'Upload',
    'chooseAvatar' => 'Choose avatar',
    'private' => 'Private',
    'save' => 'Save',
    'valids' => [
        'fullName' => 'Valid full name is required.',
        'password' => 'Valid password is required.',
        'username' => 'Your username is required.',
        'country' => 'Please select a valid country',
        'state' => 'Please provide a valid state.',
        'city' => 'Please provide a valid city.',
    ],
];
