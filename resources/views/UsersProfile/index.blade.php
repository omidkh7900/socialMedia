@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-start p-2 border-bottom border-secondary">
        <div class="w-25 h-25">
            <img class="img-thumbnail rounded-circle w-100 h-100" src="{{ route('images', ['id'=>$data['user']['id'], 'where'=>'avatar']) }}" alt='this is defualt'>
        </div>
        <div class='ml-3 w-75'>
            <div class='d-flex justify-content-around'>
                <h2>{{$data['user']['name']}}</h2>
                <a href='.' class='text-secondary btn'><h4>{{ __('userProfile.following') }}: {{$data['followingCount']}}</h4></a>
                <a href='.' class='text-secondary btn'><h4>{{ __('userProfile.followers') }}: {{$data['followersCount']}}</h4></a>
                <h4 class='text-secondary'>{{ __('userProfile.show.'.$data['show']) }}: {{$data['postsCount']}}</h4>
            </div>
            <h5 class='font-weight-bold mt-3'>{{ __('userProfile.bio') }}</h5>
            @if(!is_null($data['user']['bio']))
            <p>{{$data['user']['bio']}}</p>
            @endif
        </div>

    </div>

        <div class='text-center m-1'>
            <form method="get" action="{{route('usersProfile.index')}}">
                <input class='btn btn-secondary btn-lg w-25' type="submit" name="show" value="posts"/>
                <input class='btn btn-secondary btn-lg w-25' type="submit" name="show" value="tags"/>
            </form>
        </div>
    @if(!is_null($data['posts']))
        <div class="rounded border border-secondary border-bottom-0 p-2 d-flex justify-content-around flex-wrap">
            @foreach($data['posts'] as $post)
                <div class="card text-white bg-dark" style="width: 18rem;">
                <img class="card-img-top" src="{{ route('images', ['where'=>'post','id'=>$post->id]) }}" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text mh-25">{{$post->description}}</p>
                </div>
            </div>
            @endforeach
        </div>
    @endif
</div>
@endsection
