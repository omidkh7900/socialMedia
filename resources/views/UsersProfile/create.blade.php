@extends('layouts.app')

@section('content')
    <div class="container">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="text-center p-4 order-md-1">
            <div class="text-center d-inline-block w-25 h-25 mw-25 mh-25">
                <img class="img-thumbnail rounded-circle well" src="{{ route('images', ['id'=>$data['user']['id'], 'where'=>'avatar']) }}" alt='this is defualt'>
            </div>
            <form class="needs-validation" action='{{ route('usersProfile.update') }}' enctype="multipart/form-data" method='post'>
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="fullname">{{ __('userProfile.fullName')  }}</label>
                        <input type="text" name="fullname" class="form-control" id="fullname" value="{{ $data['user']['name'] }}">
                        <div class="invalid-feedback">
                            {{ __('userProfile.valids.fullName')  }}
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="password">{{ __('userProfile.password') }}</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="*****************">
                        <div class="invalid-feedback">
                            {{ __('userProfile.valids.password')  }}
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="username">{{ __('userProfile.username')  }}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text">@</span>
                        </div>
                        <input type="text" name="username" class="form-control" id="username" placeholder="{{ $data['user']['username'] }}">
                        <div class="invalid-feedback w-100">
                        {{ __('userProfile.valids.username')  }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5 mb-3">
                        <label for="country">{{ __('userProfile.country')  }}</label>
                        <select name="country" class="custom-select d-block w-100" id="country">
                            <option value="">{{ __('userProfile.choose')  }}</option>
                            <option>{{ __('userProfile.countries.iran')  }}</option>
                            <option selected>{{ $data['user']['country'] }}</option>
                        </select>
                        <div class="invalid-feedback">
                        {{ __('userProfile.valids.country')  }}
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="state">{{ __('userProfile.state')  }}</label>
                        <select name="state" class="custom-select d-block w-100" id="state">
                            <option value="">{{ __('userProfile.choose')  }}</option>
                            <option>{{ __('userProfile.states.tehran')  }}</option>
                            <option>{{ __('userProfile.states.khorasanRazavi')  }}</option>
                            <option selected>{{ $data['user']['state'] }}</option>
                        </select>
                        <div class="invalid-feedback">
                           {{ __('userProfile.valids.state') }}
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="city">{{ __('userProfile.city')  }}</label>
                        <select name="city" class="custom-select d-block w-100" id="city">
                            <option value="">{{ __('userProfile.choose')  }}</option>
                            <option>{{ __('userProfile.cities.tehran')  }}</option>
                            <option>{{ __('userProfile.cities.sabzevar')  }}</option>
                            <option selected>{{ $data['user']['city'] }}</option>
                        </select>
                        <div class="invalid-feedback">
                            {{ __('userProfile.valids.city') }}
                        </div>
                    </div>
                    <div class="input-group mx-3 mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">{{ __('upload') }}</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" name="avatar" class="custom-file-input" id="avatars">
                            <label class="custom-file-label" for="avatars">{{ __('userProfile.chooseAvatar')  }}</label>
                        </div>
                    </div>
                    <div class="input-group w-25 mx-3 mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" name="status" value='private' {{ $data['user']['status'] == 'public' ?:'checked' }}>
                            </div>
                        </div>
                        <p class='form-control'>{{ __('userProfile.private') }}</p>
                    </div>
                </div>
                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">{{ __('userProfile.save')  }}</button>
            </form>
        </div>
    </div>
@endsection
