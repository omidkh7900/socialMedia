<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'bio',
        'username',
        'pic',
        'country',
        'state',
        'city',
        'status',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setStatusAttribute($value)
    {
        switch ($value) {
            case 'suspend':
                $this->attributes['status'] = 1;
            break;
            case 'private':
                $this->attributes['status'] = 2;
            break;
            case 'public':
                $this->attributes['status'] = 3;
            break;
            default:
            break;
        }
    }

    public function getStatusAttribute()
    {
        switch ($this->attributes['status']) {
            case 1:
                return 'suspend';
            break;
            case 2:
                return 'private';
            break;
            case 3:
                return 'public';
            break;
            default:
            break;
        }
    }

    public function following()
    {
        return $this->belongsToMany(User::class, 'user_user', 'user_id', 'target_user')->wherePivot('status', 'follow');
    }
    public function followers()
    {
        return $this->belongsToMany(User::class, 'user_user', 'target_user', 'user_id')->wherePivot('status', 'follow');
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    public function savePosts()
    {
        return $this->belongsToMany(Post::class, 'save_posts');
    }
    public function tagsPost()
    {
        return $this->morphedByMany(Post::class, 'taggables');
    }
}
