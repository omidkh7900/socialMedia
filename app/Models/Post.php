<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
      'title',
      'description',
      'path',
    ];

    public function user()
    {
        return $this->belonsToMany(User::class);
    }
    public function tags()
    {
        return $this->morphToMany(User::class, 'taggables');
    }
}
