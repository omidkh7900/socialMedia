<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\UsersRepositoriesInterface;
use Illuminate\Support\Facades\Storage;

class UsersRepositories implements UsersRepositoriesInterface
{
    public $user;

    public function getUser()
    {
        return $this->user->toArray();
    }
    public function setUser($user)
    {
        $this->user = $user;
    }
    public function updateUser($data)
    {
        if ($this->setAndNotNullData($data['fullname'])) {
            $this->user->name = $data['fullname'];
        }
        if ($this->setAndNotNullData($data['name'])) {
            $this->user->username = $data['name'];
        }
        if ($this->setAndNotNullData($data['password'])) {
            $this->user->password = $data['password'];
        }
        if ($this->setAndNotNullData($data['country'])) {
            $this->user->country = $data['country'];
        }
        if ($this->setAndNotNullData($data['state'])) {
            $this->user->state = $data['state'];
        }
        if ($this->setAndNotNullData($data['city'])) {
            $this->user->city = $data['city'];
        }

        $this->user->status = isset($data['status']) ? $data['status'] : 'public';

        if ($this->setAndNotNullData($data['avatar'])) {
            Storage::delete($this->user->pic);
            $path = $data->file('avatar')->store('avatars');
            $this->user->pic = $path;
        }

        $this->user->save();
    }
    private function setAndNotNullData($data)
    {
        return isset($data) && !is_null($data);
    }
    public function findUserById($userId)
    {
        $this->setUser(User::findOrFail($userId));
    }
    public function followingCount()
    {
        return $this->user->following()->count();
    }
    public function followersCount()
    {
        return $this->user->followers()->count();
    }
    public function posts()
    {
        return $this->user->posts;
    }
    public function postsCount()
    {
        return $this->user->posts()->count();
    }
    public function tagsPost()
    {
        return $this->user->tagsPost;
    }
    public function tagsCount()
    {
        return $this->user->tagsPost()->count();
    }
    public function savePosts()
    {
        return $this->user->savePosts;
    }
}
