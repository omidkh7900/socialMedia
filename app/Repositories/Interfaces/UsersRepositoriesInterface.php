<?php

namespace App\Repositories\Interfaces;

interface UsersRepositoriesInterface
{
    public function followingCount();

    public function followersCount();

    public function findUserById($userId);

    public function savePosts();

    public function posts();

    public function postsCount();

    public function tagsPost();

    public function tagsCount();

    public function getUser();

    public function setUser($user);

    public function updateUser($data);
}
