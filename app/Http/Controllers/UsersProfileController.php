<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\UsersRepositoriesInterface;
use App\Http\Requests\UserProfileRequests;

class UsersProfileController extends Controller
{
    private $userRepository;

    public function __construct(UsersRepositoriesInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function index(UserProfileRequests $request)
    {
        $this->userRepository->setUser(Auth()->user());

        $request['show'] = isset($request['show']) ? $request['show'] : 'posts';
        $posts = $this->posts($request['show']);
        $postsCount = count($posts);

        $data = [
            "user"  =>  $this->userRepository->getUser(),
            "followingCount" => $this->userRepository->followingCount(),
            "followersCount" => $this->userRepository->followersCount(),
            "postsCount" => $postsCount,
            "posts" =>  $posts,
            "show" => $request['show'],
        ];
        return view('UsersProfile.index')->with('data', $data);
    }
    public function update(UserProfileRequests $request)
    {
        $this->userRepository->setUser(Auth()->user());
        $this->userRepository->updateUser($request);

        return redirect()->back();
    }
    public function create()
    {
        $this->userRepository->setUser(Auth()->user());

        $data = [
            "user" => $this->userRepository->getUser(),
        ];
        return view('UsersProfile.create')->with('data', $data);
    }
    public function posts($request)
    {
        switch ($request) {
           case 'posts':
               return $this->userRepository->posts();
           break;
           case 'tags':
               return $this->userRepository->tagsPost();
           break;
           case 'savePosts':
               return $this->userRepository->savePosts();
           break;
           default:
               return $this->userRepository->posts();
           break;
        }
    }
}
