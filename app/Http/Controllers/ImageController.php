<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validate = $request->validate([
            'where'  =>  'string|required',
            'id'     =>  'integer|required',
        ]);

        $path = $this->imagePath($validate['where'], $validate['id']);

        $path = Storage::path($path);

        return response()->file($path);
    }
    public function imagePath($where, $id)
    {
        switch ($where) {
            case 'post':
                return \App\Models\Post::select('path')->where('id', $id)->first()->path;
            break;
            case 'avatar':
                return \App\Models\User::select('pic')->where('id', $id)->first()->pic;
            break;
            default:
            break;
        }
    }
}
