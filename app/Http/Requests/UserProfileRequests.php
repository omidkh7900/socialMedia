<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserProfileRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'  =>  'unique:App\Models\User,username|max:255|nullable',
            'show'      =>  [Rule::in(['savePosts', 'posts', 'tags'])],
            'fullname'  =>  'string|min:4',
            'password'  =>  'min:8|max:16|nullable',
            'country'   =>  'string|max:255|nullable',
            'state'     =>  'string|max:255|nullable',
            'city'      =>  'string|max:255|nullable',
            'image'     =>  'image|nullable',
            'status'    =>  [Rule::in(['public','private']), 'nullable'],
        ];
    }
}
