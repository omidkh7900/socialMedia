<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersProfileController;

Route::middleware('auth')->name('usersProfile.')->prefix('usersProfile')->group(function () {
    Route::get('/', [UsersProfileController::class, 'index'])->name('index');
    Route::put('/update', [UsersProfileController::class, 'update'])->name('update');
    Route::get('/create', [UsersProfileController::class, 'create'])->name('create');
});

Route::get('/images', App\Http\Controllers\ImageController::class)->name('images');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
