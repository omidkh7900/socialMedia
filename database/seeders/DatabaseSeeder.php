<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        $this->call([
            PostSeeder::class,
        ]);

        \App\Models\User::all()->each(function ($user) {
            $status = ['follow', 'block'];
            for ($i = 0; $i< rand(0, 4); $i++) {
                $user->following()->attach(\App\Models\User::all()->random(), ['status' => $status[rand(0, 1)]]);
            }
            for ($i = 0; $i < rand(1, 4) ; $i++) {
                $user->savePosts()->attach(\App\Models\Post::all()->random());
            }
        });
    }
}
